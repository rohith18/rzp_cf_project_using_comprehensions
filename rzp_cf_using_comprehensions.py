import argparse
import json
from openpyxl import load_workbook

from utils import fetching_json_data, settlement_api, extract_company_name


def main():
    matching_count, total_transaction_value = 0, 0
    args, from_date, to_date, month = parse_args()
    cust_to_onep_rzp_list, cust_to_onep_cf_list, onep_to_ret_rzp_list = fetch_json_data(from_date, to_date)
    onep_to_ret_rzp_list = [item for item in onep_to_ret_rzp_list if (item.get("source") and item.get("source").get("status") == "processed")]
    cust_to_onep_rzp_pin_ids, cust_to_onep_cf_pin_ids, onep_to_ret_rzp_pout_ids = get_ids_list(cust_to_onep_rzp_list, cust_to_onep_cf_list, onep_to_ret_rzp_list)

    settlement_ids, settlements_list = get_settlement_ids(onep_to_ret_rzp_list)

    matching_rpin_rpout_ids = [item for item in cust_to_onep_rzp_pin_ids if item in onep_to_ret_rzp_pout_ids]
    matching_cfpin_rpout_ids = [item for item in cust_to_onep_cf_pin_ids if item in settlement_ids]

    matching_rpin_rpout = matched_rpin_rpout(cust_to_onep_rzp_pin_ids, onep_to_ret_rzp_list)
    matching_cfpin_rpout = matched_cfpin_rpout(settlements_list, cust_to_onep_cf_pin_ids)

    total_transaction_value = calculate_total_transaction_value(total_transaction_value, matching_rpin_rpout, matching_cfpin_rpout)
    print("total_transaction_value", total_transaction_value)

    matching_rpin_rpout_cid_list = extract_cid_list(matching_rpin_rpout)
    matching_cfpin_rpout_cid_list = extract_cid_list(matching_cfpin_rpout)

    matching_rpin_rpout_cname_list = extract_company_name(matching_rpin_rpout_cid_list)
    matching_cfpin_rpout_cname_list = extract_company_name(matching_cfpin_rpout_cid_list)

    unmatching_rpin_rpout = unmatched_rpin_rpout(cust_to_onep_rzp_list, onep_to_ret_rzp_pout_ids)
    unmatching_cfpin_rpout = unmatched_cfpin_rpout(cust_to_onep_rzp_list, onep_to_ret_rzp_pout_ids)
    unmatching_pout = unmatched_rpout(onep_to_ret_rzp_list, cust_to_onep_rzp_pin_ids, settlements_list, cust_to_onep_cf_pin_ids)
    write_to_excel(args, month, total_transaction_value, matching_rpin_rpout, matching_cfpin_rpout, matching_rpin_rpout_cname_list, matching_cfpin_rpout_cname_list, unmatching_rpin_rpout, unmatching_cfpin_rpout, unmatching_pout)

# Parsing arguments to get the dates
def parse_args():

    parser = argparse.ArgumentParser()
    parser.add_argument("--start-month", type=int, required=True)
    parser.add_argument("--start-day", type=int, required=True)
    parser.add_argument("--num-days", type=int, required=True)

    args = parser.parse_args()
    base_time_stamp, month = get_base_time_stamp(args.start_month)
    from_date = base_time_stamp + (args.start_day - 1)*86400
    to_date = from_date + args.num_days*(86400)

    return args, from_date, to_date, month


# Base time stamp gives the unix time stamp for a specific month
def get_base_time_stamp(start_month):
    month =""
    if start_month == 1:
        month = "January"
        base_time_stamp = 1640995200 # January 1st
    if start_month == 2:
        month = "February"
        base_time_stamp = 1643673600 # February 1st
    if start_month == 3:
        month = "March"
        base_time_stamp = 1646092800 # March 1st
    if start_month == 4:
        month = "April"
        base_time_stamp = 1648771200 # April 1st

    return base_time_stamp, month


# Fetching json data of razor payin, cashfree payin and razor payout
def fetch_json_data(from_date, to_date):

    CUST_TO_ONEP_RZP_GET_URL = "https://api.pharmacyone.io/prod/rzp_transaction"
    CUST_TO_ONEP_CF_POST_URL = "https://api.pharmacyone.io/prod/cf_payments"
    ONEP_TO_RET_RZP_GET_URL = "https://api.pharmacyone.io/prod/rzp_payout"

    ONEP_TO_RET_RZP_GET_HEADERS, CUST_TO_ONEP_RZP_GET_HEADERS = {"session-token": "wantednote"}, {"session-token": "wantednote"}
    CUST_TO_ONEP_CF_POST_HEADERS = {"session-token": "wantednote", "Content-Type": "application/json"}

    CUST_TO_ONEP_CF_BODY = json.dumps({"startDate": from_date, "endDate": to_date})
    CUST_TO_ONEP_RZP_BODY, ONEP_TO_RET_RZP_BODY = None, None

    cust_to_onep_rzp_list = fetching_json_data(CUST_TO_ONEP_RZP_GET_URL, CUST_TO_ONEP_RZP_GET_HEADERS, CUST_TO_ONEP_RZP_BODY, from_date, to_date)
    cust_to_onep_cf_list = fetching_json_data(CUST_TO_ONEP_CF_POST_URL, CUST_TO_ONEP_CF_POST_HEADERS, CUST_TO_ONEP_CF_BODY, from_date, to_date)
    onep_to_ret_rzp_list = fetching_json_data(ONEP_TO_RET_RZP_GET_URL, ONEP_TO_RET_RZP_GET_HEADERS, ONEP_TO_RET_RZP_BODY, from_date, to_date )

    return cust_to_onep_rzp_list, cust_to_onep_cf_list, onep_to_ret_rzp_list


def get_ids_list(cust_to_onep_rzp_list, cust_to_onep_cf_list, onep_to_ret_rzp_list):
    cust_to_onep_rzp_pin_ids, cust_to_onep_cf_pin_ids, onep_to_ret_rzp_pout_ids = [], [], []
    
    cust_to_onep_rzp_pin_ids = [item.get("id") for item in cust_to_onep_rzp_list]
    cust_to_onep_cf_pin_ids = [str(item.get("referenceId")) for item in cust_to_onep_cf_list]
    onep_to_ret_rzp_pout_ids = [item.get("source").get("notes").get("id") for item in onep_to_ret_rzp_list if (item.get("source") and item.get("source").get("notes") and item.get("source").get("notes").get("id"))]

    return cust_to_onep_rzp_pin_ids, cust_to_onep_cf_pin_ids, onep_to_ret_rzp_pout_ids


# Getting settlement lists
def get_settlements_list(onep_to_ret_rzp_list):
    settlements_list = []
    final_settlements_list = []

    for item in onep_to_ret_rzp_list:
        if (item.get("source") and item.get("source").get("notes") and item.get("source").get("notes").get("type") and item.get("source").get("notes").get("type") == "settlement"):
            settlements_list = settlement_api(item.get("source").get("notes").get("id"))
            for item in settlements_list:
                final_settlements_list.append(item)

    return final_settlements_list


# Getting settlement ids
def get_settlement_ids(onep_to_ret_rzp_list):
    settlements_list = get_settlements_list(onep_to_ret_rzp_list)
    settlement_ids = []
    settlement_ids = [item.get("id") for item in settlements_list]
    
    return settlement_ids, settlements_list


# Matching razor payin and razor payout
def matched_rpin_rpout(cust_to_onep_rzp_pin_ids, onep_to_ret_rzp_list):
    matching_rpin_rpout = {}
    matching_rpin_rpout = {item.get("source").get("notes").get("id"): [item.get("source").get("notes").get("id"), item.get("source").get("notes").get("id"), item.get("source").get("utr"), int(item.get("amount")/ 100), item.get("source").get("notes").get("cid"), item.get("settlementId"), item.get("created_at")] for item in  onep_to_ret_rzp_list if(item.get("source") and item.get("source").get("notes") and item.get("source").get("notes").get("id") and item.get("source").get("notes").get("id") in cust_to_onep_rzp_pin_ids)}

    return matching_rpin_rpout


# Matching cashfree payin and razor payout
def matched_cfpin_rpout(settlements_list, cust_to_onep_cf_pin_ids):
    matching_cfpin_rpout = {}
    matching_cfpin_rpout = {item.get("id"): [item.get("id"), item.get("id"), item.get("utr"), item.get("amount"), item.get("cid"), item.get("settlementId"), item.get("paymentTime")] for item in settlements_list if item.get("id") in cust_to_onep_cf_pin_ids}

    return matching_cfpin_rpout


def calculate_total_transaction_value(total_transaction_value, matching_rpin_rpout, matching_cfpin_rpout):
    matching_lists = [matching_rpin_rpout, matching_cfpin_rpout]

    for list_01 in matching_lists:
        for key, value in list_01.items():
            item = list_01.get(key)
            if item[3]:
                if list_01 == matching_rpin_rpout:
                    total_transaction_value += item[3]/ 100  # Dividing by 100 to convert paise into Rupees
                if list_01 == matching_cfpin_rpout:
                    total_transaction_value += item[3]
        
    return total_transaction_value


def extract_cid_list(in_list):
    out_list = []
    out_list = [in_list.get(key)[4] for key, value in in_list.items()]

    return out_list


def unmatched_rpin_rpout(cust_to_onep_rzp_list, onep_to_ret_rzp_pout_ids):
    unmatching_rpin_rpout = {}
    unmatching_rpin_rpout = {item.get("id"): [item.get("id"), item.get("utr"), item.get("amount"), item.get("notes").get("cid"), "", item.get("created_at")] for item in  cust_to_onep_rzp_list if item.get("id") not in onep_to_ret_rzp_pout_ids}

    return unmatching_rpin_rpout


def unmatched_cfpin_rpout(cust_to_onep_cf_list, settlement_ids):
    unmatching_cfpin_rpout = {}
    unmatching_cfpin_rpout = {item.get("referenceId"): [item.get("referenceId"), item.get("utr"), float(item.get("amount")), item.get("cid"), "", item.get("paymentTime")] for item in cust_to_onep_cf_list if item.get("id") not in settlement_ids}

    return unmatching_cfpin_rpout


def unmatched_rpout(onep_to_ret_rzp_list, cust_to_onep_rzp_pin_ids, settlements_list, cust_to_onep_cf_pin_ids):
    unmatching_rpout_with_rpin = {item.get("source").get("notes").get("id"): [item.get("source").get("notes").get("id"), item.get("utr"), (item.get("amount")/ 100), item.get("cid"), "", item.get("created_at")] for item in  onep_to_ret_rzp_list if(item.get("source") and item.get("source").get("notes") and item.get("source").get("notes").get("id") and item.get("source").get("notes").get("id") not in cust_to_onep_rzp_pin_ids)}
    unmatched_rpout_with_cf = {item.get("id"): [item.get("id"), item.get("utr"), item.get("amount"), item.get("cid"), "", item.get("paymentTime")] for item in settlements_list if item.get("id") not in cust_to_onep_cf_pin_ids}
    unmatching_pout = {**unmatching_rpout_with_rpin, **unmatched_rpout_with_cf}

    return unmatching_pout


# Writing to excel sheet
def write_to_excel(args, month, total_transaction_value, matching_rpin_rpout, matching_cfpin_rpout, matching_rpin_rpout_cname_list, matching_cfpin_rpout_cname_list, unmatching_rpin_rpout, unmatching_cfpin_rpout, unmatching_pout):
    workbook = load_workbook("rzp_cf.xlsx")

    writing_matched_info(args, month, matching_rpin_rpout, matching_cfpin_rpout, matching_rpin_rpout_cname_list, matching_cfpin_rpout_cname_list, workbook)
    writing_unmatched_pin_info(workbook, args, month, unmatching_rpin_rpout, unmatching_cfpin_rpout)
    writing_unmatched_pout_info(workbook, args, month, unmatching_pout)
    writing_summary(args, month, total_transaction_value, workbook)

    workbook.save("rzp_cf.xlsx")


def writing_matched_info(args, month, matching_rpin_rpout, matching_cfpin_rpout, matching_rpin_rpout_cname_list, matching_cfpin_rpout_cname_list, workbook):
    matched_info_sheet = workbook.create_sheet(f"from{month}{args.start_day}(NoOfDays-{args.num_days})")
    matched_info_sheet.append(["pay_in", "pay_out", "utr", "amount (in Rupees)", "cid", "settlement_id", "created_at", "cname"])

    for key, value in matching_rpin_rpout.items():
        matched_info_sheet.append(matching_rpin_rpout[key])

    for key, value in matching_cfpin_rpout.items():
        matched_info_sheet.append(matching_cfpin_rpout[key])


def writing_unmatched_pin_info(workbook, args, month, unmatching_rpin_rpout, unmatching_cfpin_rpout):
    unmatched_pin_info_sheet = workbook.create_sheet(f"from{month}{args.start_day}(NoOfDays-{args.num_days})(unmatched_pin)")
    unmatched_pin_info_sheet.append(["pay_in", "pay_out", "utr", "amount (in Rupees)", "cid", "settlement_id", "created_at", "cname"])

    for key, value in unmatching_rpin_rpout.items():
        unmatched_pin_info_sheet.append(unmatching_rpin_rpout[key])

    for key, value in unmatching_cfpin_rpout.items():
        unmatched_pin_info_sheet.append(unmatching_cfpin_rpout[key])


def writing_unmatched_pout_info(workbook, args, month, unmatching_pout):
    unmatched_pout_info_sheet = workbook.create_sheet(f"from{month}{args.start_day}(NoOfDays-{args.num_days})(unmatched_pout)")
    unmatched_pout_info_sheet.append(["pay_in", "pay_out", "utr", "amount (in Rupees)", "cid", "settlement_id", "created_at", "cname"])

    for key, value in unmatching_pout.items():
        unmatched_pout_info_sheet.append(unmatching_pout[key])


def writing_summary(args, month, total_transaction_value, workbook):
    summary_sheet = workbook["summary"]
    summary_sheet.append([f"from{month}{args.start_day}(NoOfDays-{args.num_days})", total_transaction_value])


if __name__ == "__main__":
    main()
