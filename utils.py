import requests
import datetime
import copy
import json


def get_last_return_id(url, headers, from_date, to_date, last_return_id):
    body = json.dumps({"startDate": from_date, "endDate": to_date, "lastReturnId": last_return_id})
    response = requests.post(url, headers=headers, data=body)
    response_body = response.json()
    last_return_id = response_body.get("data").get("lastReturnId")

    return last_return_id

def get_response_list(url, headers, body, from_date, to_date, last_return_id):
    last_return_ids_list = []
    response_list = []

    for i in range(10):
        last_return_id = get_last_return_id(url, headers, from_date, to_date, last_return_id)
        last_return_ids_list.append(last_return_id)

    last_return_ids_list = copy.deepcopy(list(set(last_return_ids_list)))

    for i in last_return_ids_list:
        if i == None:
            last_return_ids_list.remove(i)

    for last_return_id in last_return_ids_list:
        body = json.dumps({"startDate": from_date, "endDate": to_date, "lastReturnId": last_return_id})
        response = requests.post(url, headers=headers, data=body)
        response_body = response.json()
        response_list += response_body.get("data").get("payments")

    return response_list

def unix_timestamp_to_date(in_list):
    out_list = []
    for item in in_list:
        value = datetime.datetime.fromtimestamp(item)
        out_list.append(f"{value:%Y/%m/%d %H:%M:%S}")

    return out_list

def removing_duplicates(list_name):
    return list(set(list_name))

def fetching_json_data(url, headers, body, from_date, to_date):
    response_list = []
    final_response_list = []
    for skip in range(0, 100000, 100):
        if body:
            response = requests.post(url, headers=headers, data=body)
            response_body = response.json()
            try:
                response_list = response_body.get("data").get("payments")
                last_return_id = response_body.get("data").get("lastReturnId")
                response_list += get_response_list(url, headers, json.dumps({"startDate": from_date, "endDate": to_date, "lastReturnId": last_return_id}), from_date, to_date, last_return_id)
                final_response_list += response_list
            except AttributeError:
                pass
            return final_response_list
        else:
            payload = {"skip":skip, "from": from_date, "to": to_date} 
            response = requests.get(url, params=payload, headers=headers)
            response_body = response.json()
            try:
                response_list = response_body.get("data").get("items")
            except AttributeError:
                pass
        if len(response_list) == 0:
            break
        final_response_list = final_response_list + response_list

    return final_response_list

def extract_company_name(in_list):
    out_list = []
    headers = {"session-token": "wantednote"}
    
    for cid in in_list:
        url = f"https://api.pharmacyone.io/prod/company_details/{cid}"
        response = requests.get(url, headers=headers)
        response_body = response.json()
        try:
            if response_body.get("data").get("Company").get("id"):
                out_list.append(response_body.get("data").get("Company").get("name"))
        except AttributeError:
            out_list.append("")
        
    return out_list

def settlement_api(id):
    out_list = []
    url = f"https://api.pharmacyone.io/prod/settlement_transaction/{id}"
    headers = {"session-token": "wantednote"}
    response = requests.get(url, headers=headers)
    response_body = response.json()
    try:
        out_list = response_body.get("data").get("items")
    except AttributeError:
        pass

    return out_list


